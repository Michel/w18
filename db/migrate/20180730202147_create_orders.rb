class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :email
      t.integer :packages
      t.string :firstname
      t.string :lastname
      t.string :street
      t.integer :postcode
      t.string :city

      t.integer :payment_method
      t.json :payment_details

      t.timestamps
    end
  end
end
