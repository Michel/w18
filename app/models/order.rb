class Order < ApplicationRecord
  CCARD_ISSUER = [["master", "Master Card"], ["visa", "Visa"]]
  has_many :messages, dependent: :destroy

  enum payment_method: [:sepa, :amazon, :paypal, :credit_card]

  attribute :auto_translate, :boolean, default: true
  attribute :iban, :string
  attribute :bic, :string
  attribute :cc_owner, :string
  attribute :cc_issuer, :string
  attribute :cc_number, :string
  attribute :cc_checksum, :integer
  attribute :cc_valid_till_month, :integer
  attribute :cc_valid_till_year, :integer

end
