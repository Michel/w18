/* eslint no-console:0 */

import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import '../../assets/stylesheets/app.scss';
import 'images/raw1.jpeg'
import 'images/raw2.jpeg'
import 'images/raw3.jpeg'
import 'images/SepaLogoEN.jpg'

import Materialize from 'materialize-css/dist/js/materialize';
import '@fortawesome/fontawesome-free/css/all.min';

const application = Application.start()
const context = require.context("controllers", true, /.js$/)
application.load(definitionsFromContext(context))

document.addEventListener("DOMContentLoaded", function() {
    var elems = document.querySelectorAll('.parallax');
    var instances = Materialize.Parallax.init(elems);
});