
import {Controller} from "stimulus"
import Materialize from 'materialize-css/dist/js/materialize';


export const price = 20;

export default class extends Controller {
    static targets = [ "packages", "sum", "message", "payment", "ccIssuer" ];

    initialize() {
        Materialize.textareaAutoResize(this.messageTarget);
        var paymentTabs = Materialize.Tabs.init(this.paymentTarget, {swipeable: true});
        var ccardIssuer = Materialize.FormSelect.init(this.ccIssuerTarget);
    }

    connect() {
        this.packagesTarget.value = parseInt(this.element.getAttribute("data-packages"));
        this.sumTarget.textContent = price * this.packagesTarget.value;
    }

    more() {
        this.packagesTarget.value++;
        this.sumTarget.textContent = price * this.packagesTarget.value;
    }
    less() {
        this.packagesTarget.value = Math.max(1, this.packagesTarget.value-1);
        this.sumTarget.textContent = price * this.packagesTarget.value;
    }
}
